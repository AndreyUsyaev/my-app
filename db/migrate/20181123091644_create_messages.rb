class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.references :conversation, index: true
      t.references :user, index: true
      t.text :body, default: ''
    end
  end
end
