class CreateConversations < ActiveRecord::Migration[5.2]
  def change
    create_table :conversations do |t|
      t.references :sender
      t.references :recipient
      t.index [:sender_id, :recipient_id], unique: true

      t.timestamps
    end
  end
end
