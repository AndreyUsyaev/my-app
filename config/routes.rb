Rails.application.routes.draw do
  mount ActionCable.server => '/cable'
  devise_for :users, controllers: {registrations: 'users/registrations', sessions: 'users/sessions'}
  root 'main#index'
  resources :conversations, :users, :messages
end
