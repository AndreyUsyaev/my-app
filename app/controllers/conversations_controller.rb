class ConversationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_conversation, only: [:show]

  def index
    @conversations = Conversation.where(sender_id: current_user.id)
                         .or(Conversation.where(recipient_id: current_user.id))
  end

  def show
    @messages = @conversation.messages
  end

  def create
    @recipient = User.find(params[:recipient_id])
    @conversation = Conversation.create(sender_user: current_user,
                                        recipient_user: @recipient)
    redirect_to conversation_path(@conversation)
  end

  private

  def set_conversation
    @conversation = Conversation.where(sender_id: current_user.id)
                        .or(Conversation.where(recipient_id: current_user.id)).first
  end
end
