class UsersController < ApplicationController
  before_action :set_user, only: [:show]

  def show
    @conversation = Conversation.where(sender_id: @user.id)
                        .or(Conversation.where(recipient_id: @user.id)).first_or_initialize
  end

  private

  def set_user
    @user = User.find(params[:id])
  end
end
