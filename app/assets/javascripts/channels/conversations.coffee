$(document).on 'turbolinks:load', ->
  $messages = $('.messages-block:first')
  $form = $('#new_message')
  $message_field = $('#message_body', $form)
  if $form.length > 0
    App.global_chat = App.cable.subscriptions.create {
      channel: "ConversationChannel"
      conversation_id: $('#message_conversation_id', $form).val()
    },
      connected: ->
        # Called when the subscription is ready for use on the server

      disconnected: ->
        # Called when the subscription has been terminated by the server

      received: (data) ->
        if data['message']
          $message_field.val('')
          $messages.append(data['message'])
          if $('p', $('.messages-block:first')).length > 0
            removeEmptyLabel()

      send_message: (message, conversation_id) ->
        @perform 'send_message', body: message, conversation_id: conversation_id

    $form.submit (e) ->
      $this = $(this)
      if $.trim($message_field.val()).length > 1
        App.global_chat.send_message $message_field.val(), $('#message_conversation_id', $form).val()
      e.preventDefault()
      return false

removeEmptyLabel = ->
  $('p', $('.messages-block:first')).remove()