namespace 'App.Flash'

class App.Flash
  @bootstrap_class_for: (flash_type) ->
    {
      success: 'alert-success'
      error: 'alert-danger'
      alert:  'alert-warning'
      notice: "alert-info"
    }[flash_type] || flash_type

  @format_html: (type, message) ->
    "<div class='uk-notify-message alert #{@bootstrap_class_for(type)}'>
        <button class='close' data-dismiss='alert'>×</button>
        <div>#{message}</div>
    </div>"

  @_append: (html) ->
    $('.uk-notify').append(html)

  @html: (message) ->
    @_append(message)

  @send: (type, message) ->
    @_append(@format_html(type, message))

  @success: (message) ->
    @send('success', message)

  @error: (message) ->
    @send('error', message)

  @alert: (message) ->
    @send('alert', message)

  @notice: (message) ->
    @send('notice', message)