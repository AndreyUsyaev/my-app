$(document).on 'turbolinks:load', ->
  $(window).trigger('resize')

$(window).on 'resize', ->
  windowHeight = $(window).height()
  headerHeight = $('.container-header').height()
  footerHeight = $('.container-footer').height()
  mainHeight = windowHeight - headerHeight - footerHeight
  $('.container-main').css({minHeight: mainHeight})