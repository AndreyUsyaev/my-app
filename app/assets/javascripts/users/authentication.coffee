$(document).on 'turbolinks:load', ->

  $('form#new_user_modal').submit ->
    $form = $(this)
    $.ajax
      type: $form.attr('method')
      url: $form.attr('action')
      data: $form.serialize()
      dataType: $form.data('type')
      success: (data, status) ->
        Turbolinks.clearCache()
        $('#signUpModal').modal('hide')
        $(document).one 'turbolinks:render', ->
          App.Flash.notice('Successfully sign up.')
        Turbolinks.visit(window.location.pathname)
      error: (data) ->
        div = document.createElement('div')
        errors = $.parseJSON(data.responseText).errors
        for key of errors
          $(div).append('<li>' + key.substr(0,1).toUpperCase()+key.substr(1) + ' ' + errors[key]  + '</li>')
        App.Flash.error($(div).html())
    false

  $('form#login_modal').submit ->
    $form = $(this)
    $.ajax
      type: $form.attr('method')
      url: $form.attr('action')
      data: $form.serialize()
      dataType: $form.data('type')
      success: (data, status) ->
        Turbolinks.clearCache()
        $('#loginModal').modal('hide')
        $(document).one 'turbolinks:render', ->
          App.Flash.notice('Successfully sign in.')
        Turbolinks.visit(window.location.pathname)
      error: (data) ->
        App.Flash.error($.parseJSON(data.responseText).error)
    false