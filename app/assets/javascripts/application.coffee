#= require jquery3
#= require rails-ujs
#= require bootstrap
#= require turbolinks
#= require_tree ./helpers/
#= require_tree ./users/
#= require_tree ./app/
#= require_tree .

namespace 'App'