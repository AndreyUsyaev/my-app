class ConversationChannel < ApplicationCable::Channel
  def subscribed
    stream_from"conversation_#{params['conversation_id']}_channel"
  end

  def unsubscribed

  end

  def send_message(data)
    current_user.messages.create!(body: data['body'], conversation_id: data['conversation_id'])
  end
end