module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
       set_current_user
    end

    private

    def set_current_user
      (self.current_user = env['warden'].user) || reject_unauthorized_connection
    end
  end
end
