class Conversation < ApplicationRecord
  has_many :messages, inverse_of: :conversation
  belongs_to :sender_user, class_name: 'User', foreign_key: 'sender_id'
  belongs_to :recipient_user, class_name: 'User', foreign_key: 'recipient_id'

  def recipient(user_id)
    self.sender_id == user_id ? self.sender_user : self.recipient_user
  end
end
