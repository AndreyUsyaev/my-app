class Message < ApplicationRecord
  belongs_to :conversation, inverse_of: :messages
  belongs_to :user, inverse_of: :messages

  after_create_commit { MessageBroadcastJob.perform_later(self) }
end
